# DisqusCircle
A Disqus System written using node.js / express.js / mongodb / passport.js

### Features:
* Discussion-Page
* Submitting comments / posts
* Voting on posts / comments
* Sorting
* Profile pages
* Relative time
* Validation
* Login / Register
* Hash / salted passwords
* Change password / delete account
* API
* handled pagination in api-side

for showing pagination you can check it in perticular post's comment page you can see only 5 comments, due to pagination only 5 comments you can see based on sorting method

Import Data - 

You can import whole dumped database into your local machine using mongodb studio 3t / MongoDB Compass, which is required to run in your local machine.

for dump - mongodump --db disqus-system
I used this command, for using mongorestore command you can restore the database.

If you don't import any thing still it will works, as you running the app it will filling with data.

Use a user only if you have imported database

username - user
password - user

or create your own by registering in a form which takes only username and password in registration form.

## run app

` npm install `

then run using below command.

` npm start / node app.js `