module.exports.config = {
    uri: 'mongodb://localhost:27017/disqus_system',
    options: {
        dbName: 'disqus-system',
        useNewUrlParser: true
    },
}