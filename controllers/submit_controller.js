let Topic = require("../models/topic");
let Post = require("../models/post");
let Profile = require("../models/profile");

exports.topic_post_view = function (req, res) {
    let subscribed = false
    let karma = 0

    Profile.find({
        username: req.session.user
    }, function (err, result) {
        if (err) throw err;

        if (result.length) {
            karma = result[0]['karma_post'] + result[0]['karma_comment']
        }
    });

    Profile.find({
        username: req.session.user,
        subscribed: req.params.topic,
    }, function (err, doc) {
        if (err) throw err;

        if (!doc.length) {
            // res.send("Unable to find topic state")
            return;
        } else {
            subscribed = true
        }
    }).then(function () {
        Topic.find({
            name: req.params.topic
        }, function (err, doc) {
            if (err) throw err

            if (doc.length) {
                res.render('./topic/topic_post', {
                    info: doc[0],
                    karma: karma,
                    state: subscribed,
                    isAuth: req.isAuthenticated(),
                })
            }
        })
    })
}

exports.topic_post = function (req, res) {
    Post({
        title: req.body.title,
        body: req.body.body,
        username: req.session.user,
        type: "post",
        topic: req.params.topic,
    }).save(function (err, doc) {
        if (err) throw err;

        console.log(`[${req.params.topic}] post submitted!`)
        res.redirect(`/r/${req.params.topic}`)
    })
}

// SUBMITING A POST
exports.front_post = function (req, res) {
    Post({
        title: req.body.title,
        body: req.body.text,
        username: req.session.user,
        type: "post",
        topic: req.body.topic,
    }).save(function (err, doc) {
        if (err) throw err;

        console.log(`[Frontpage] post submitted to [${req.body.topic}]`)
        res.redirect(`/r/${req.body.topic}/${doc._id}/comments`);
    });
}

// SUBMITING A topic
exports.topic = function (req, res) {
    Profile.update({
            username: req.session.user
        }, {
            $push: {
                owned: req.body.topic
            }
        },
        function (err, doc) {
            if (err) throw err;

        }).then(function () {
        Topic({
            name: req.body.topic,
            description: req.body.description
        }).save(function (err, doc) {
            if (err) throw err

            console.log(`[Frontpage] ${req.body.topic} topic created`)
            res.redirect(`/r/${req.body.topic}`);
        });
    });
}

exports.front_post_view = function (req, res) {
    let subscribed = undefined;
    let karma = 0;

    Profile.find({
        username: req.session.user
    }, function (err, result) {
        if (err) throw err;

        if (result.length) {
            subscribed = result[0]['subscribed']
            karma = result[0]['karma_post'] + result[0]['karma_comment']

        }

        res.render("./front/front_post", {
            isAuth: req.isAuthenticated(),
            subscribed: subscribed,
            karma: karma
        });
    })
}

exports.front_post_view = function (req, res) {
    let subscribed = undefined;
    let karma = 0;

    Profile.find({
        username: req.session.user
    }, function (err, result) {
        if (err) throw err;

        if (result.length) {
            subscribed = result[0]['subscribed']
            karma = result[0]['karma_post'] + result[0]['karma_comment']

        }

        res.render("./front/front_post", {
            isAuth: req.isAuthenticated(),
            subscribed: subscribed,
            karma: karma
        });
    })
}

exports.topic_view = function (req, res) {
    let subscribed = undefined;
    let karma = 0;

    Profile.find({
        username: req.session.user
    }, function (err, result) {
        if (err) throw err;

        if (result.length) {
            subscribed = result[0]['subscribed']
            karma = result[0]['karma_post'] + result[0]['karma_comment']
        }

        res.render("./front/front_topic", {
            isAuth: req.isAuthenticated(),
            karma: karma,
            subscribed: result[0]['subscribed']
        });
    })
}