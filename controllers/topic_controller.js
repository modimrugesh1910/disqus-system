let Topic = require("../models/topic");
let Post = require("../models/post");
let Comment = require("../models/comment");
let Profile = require("../models/profile");

exports.get_all = function (req, res) {
    let topic = undefined;
    let posts = undefined;
    let subscribed = false;
    let karma = 0

    let sort = undefined;

    switch (req.query.sort) {
        case "top":
            sort = {
                votes: -1
            }
            break;
        case "new":
            sort = {
                time: -1
            }
            break;
        case "old":
            sort = {
                time: 1
            }
            break;
        default:
            sort = {
                votes: -1
            }
    }

    Profile.find({
        username: req.session.user
    }, function (err, result) {
        if (err) throw err;

        if (result.length) {
            karma = result[0]['karma_post'] + result[0]['karma_comment']
        }
    });

    Topic.find({
        name: req.params.topic
    }, function (err, doc) {
        if (err) throw err;

        if (doc.length) {
            topic = doc[0]
        } else {
            res.render("./error")
        }
    }).then(function () {
        Profile.find({
            username: req.session.user,
            subscribed: req.params.topic,
        }, function (err, doc) {
            if (err) throw err;

            if (!doc.length) {
                // res.send("Unable to find topic state")
                return;
            } else {
                subscribed = true
            }
        }).then(function () {
            Post.find({
                topic: req.params.topic
            }).sort(sort).exec(function (err, result) {
                if (err) throw err;
                if (result.length) {
                    posts = result
                }

                console.log(`[${req.params.topic}] fetching posts!`)
                res.render("./topic/topic", {
                    info: topic,
                    posts: posts,
                    karma: karma,
                    state: subscribed,
                    isAuth: req.isAuthenticated()
                })
            });
        });
    });
}

exports.get_post = function (req, res) {
    let info = undefined
    let post = undefined
    let comments = undefined
    let subscribed = false;
    let karma = 0

    let sort = undefined;

    switch (req.query.sort) {
        case "top":
            sort = {
                votes: -1
            }
            break;
        case "new":
            sort = {
                time: -1
            }
            break;
        case "old":
            sort = {
                time: 1
            }
            break;
        default:
            sort = {
                votes: -1
            }
    }

    let row = req.query.rows ? parseInt(req.query.rows) : 5;
    let pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 0;

    Profile.find({
        username: req.session.user
    }, function (err, result) {
        if (err) throw err;

        if (result.length) {
            karma = result[0]['karma_post'] + result[0]['karma_comment']
        }
    });

    Topic.find({
        name: req.params.topic
    }, function (err, doc) {
        if (err) throw err

        if (doc.length) {
            info = doc[0]
        }
    }).then(function () {
        Profile.find({
            username: req.session.user,
            subscribed: req.params.topic,
        }, function (err, doc) {
            if (err) throw err;

            if (!doc.length) {
                // res.send("Unable to find topic state")
                return;
            } else {
                subscribed = true
            }
        }).then(function () {
            Post.find({
                _id: req.params.id
            }, function (err, doc) {
                if (err) {
                    res.render('./error')
                } else {
                    if (doc.length) {
                        post = doc[0]
                    }
                }
            }).then(function () {

                Comment
                .find({ref: req.params.id})
                .limit(row)
                .skip(row * pageNo)
                .sort(sort)
                .exec(function (err, result) {
                    Comment.countDocuments({}).exec(function (err, count) {
                            if (err) throw err;
                            if (result.length) {
                                comments = result
                            }

                            res.render('./post', {
                                info: info,
                                post: post,
                                page: pageNo,
                                totalRows: count,
                                karma: karma,
                                comments: comments,
                                state: subscribed,
                                isAuth: req.isAuthenticated()
                            })
                        })
                    })
            })
        })
    })
}

// CHECKING topic
exports.check_topic = function (req, res) {
    Topic.find({
        name: req.params.topic
    }, function (err, doc) {
        if (err) throw err;

        if (!doc.length) {
            res.send(false);
            return;
        }
        console.log(`[${req.params.topic}] checked!`)
        res.send(true);
    });
}

// SUBSCRIBING TO topic
exports.subscribe = function (req, res) {
    Profile.updateOne({
        username: req.session.user
    }, {
        $push: {
            subscribed: req.params.topic
        }
    }, function (err, doc) {
        if (err) throw err;

        console.log(`[${req.params.topic}] subscription added!`)
        res.send('success!')
    })
}

// UNSUBSCRIBE FROM topic
exports.unsubscribe = function (req, res) {
    Profile.updateOne({
        username: req.session.user
    }, {
        $pull: {
            subscribed: req.params.topic
        }
    }, function (err, doc) {
        if (err) throw err;

        console.log(`[${req.params.topic}] subscription removed!`)
        res.send('success!')
    })
}