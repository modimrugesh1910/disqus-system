const mongoose = require('mongoose');

const postStateSchema = new mongoose.Schema({
    username: String,
    ref: mongoose.Schema.Types.ObjectId,
    vote: {
        type: String,
        default: "neutral"
    },
});

module.exports = mongoose.model('PostState', postStateSchema, "postStates");