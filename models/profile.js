const mongoose = require('mongoose');

const profileSchema = new mongoose.Schema({
    username: String,
    karma_post: {
        type: Number,
        default: 0
    },
    karma_comment: {
        type: Number,
        default: 0
    },
    subscribed: Array,
    owned: Array
});

module.exports = mongoose.model('Profile', profileSchema);