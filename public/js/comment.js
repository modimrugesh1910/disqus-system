$("document").ready(function () {
    // event handler for upvoting a comment
    $(".upvote-comment").click(function () {
        let down_arrow = $(this).parent().find(".downvote-comment")
        let query = $(this).closest('article')

        let ref = query.data('ref')
        let votes = query.find('.comment-votes')
        let comment_user = query.find('.comment-user').text()
        let counter;

        // if upvote is already toggled and user presses it again, 
        // toggle off the upvote button and decrement vote.
        if ($(this).hasClass("up-enabled")) {
            counter = votes.text();
            votes.text(--counter);
            $(this).removeClass("up-enabled");

            $.ajax({
                type: "put",
                url: `/vote/comment/${ref}`,
                data: {
                    vote: counter,
                    state: "neutral",
                    action: "decrement",
                    user: comment_user
                }
            });
            return false;
        }

        // if downvote is already toggled while upvote is pressed
        // toggle off downvote and increment vote
        if (down_arrow.hasClass('down-enabled')) {
            down_arrow.removeClass("down-enabled");
            counter = votes.text();
            votes.text(++counter);

            $.ajax({
                type: "put",
                url: `/vote/comment/${ref}`,
                data: {
                    vote: counter,
                    state: "neutral",
                    action: "increment",
                    user: comment_user
                }
            });
        }

        // if upvote isnt toggled while upvote is pressed,
        // toggle upvote and increment vote.
        else if (!$(this).hasClass("up-enabled")) {
            counter = votes.text();
            votes.text(++counter);
            $(this).addClass("up-enabled");

            $.ajax({
                type: "put",
                url: `/vote/comment/${ref}`,
                data: {
                    vote: counter,
                    state: "up",
                    action: "increment",
                    user: comment_user
                }
            });
        }
        return false;
    })

    // event handler for downvoting a comment
    $(".downvote-comment").click(function () {
        let up_arrow = $(this).parent().find(".upvote-comment")
        let query = $(this).closest('article')

        let ref = query.data('ref')
        let votes = query.find('.comment-votes')
        let comment_user = query.find('.comment-user').text()
        let counter;

        // if downvote is already toggled and user presses it again, 
        // toggle off the downvote button and increment vote.
        if ($(this).hasClass("down-enabled")) {
            counter = votes.text();
            votes.text(++counter);
            $(this).removeClass("down-enabled");

            $.ajax({
                type: "put",
                url: `/vote/comment/${ref}`,
                data: {
                    vote: counter,
                    state: "neutral",
                    action: "increment",
                    user: comment_user
                }
            });
            return false;
        }

        // if upvote is already toggled while downvote is pressed
        // toggle off upvote and decrement vote
        if (up_arrow.hasClass('up-enabled')) {
            up_arrow.removeClass("up-enabled");
            counter = votes.text();
            votes.text(--counter);

            $.ajax({
                type: "put",
                url: `/vote/comment/${ref}`,
                data: {
                    vote: counter,
                    state: "neutral",
                    action: "decrement",
                    user: comment_user
                }
            });

            // if downvote isnt toggled while downvote is pressed,
            // toggle downvote and decrement vote.
        } else if (!$(this).hasClass("down-enabled")) {
            counter = votes.text();
            votes.text(--counter);
            $(this).addClass("down-enabled");

            $.ajax({
                type: "put",
                url: `/vote/comment/${ref}`,
                data: {
                    vote: counter,
                    state: "down",
                    action: "decrement",
                    user: comment_user
                }
            });
        }
        return false;
    });
});