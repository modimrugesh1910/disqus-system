$("document").ready(function () {

    // onsubmit validation handler for when user submits a topic
    // it checks if the topic is a valid one by making a query to the database via the server
    $("#form-topic").submit(function (e) {
        e.preventDefault()

        $.ajax({
            type: "get",
            url: `/submit/check/${$("#topic_form").val()}`
        }).done(function (isvalid) {
            if (isvalid == true) {
                $('#topic_form').addClass('is-invalid')
            } else {
                $('form').unbind('submit').submit()
            }
        });
    });

    // onsubmit validation handler for when user submits a post or link
    // it checks if the topic is a valid one by making a query to the database via the server
    $("#form-post-or-link").submit(function (e) {
        e.preventDefault()

        $.ajax({
            type: "get",
            url: `/submit/check/${$("#topic_form").val()}`
        }).done(function (isvalid) {
            if (isvalid == false) {
                $('#topic_form').addClass('is-invalid')
            } else {
                $('form').unbind('submit').submit()
            }
        });
    });
});