const express = require("express");
const router = express.Router();

// CONTROLLERS
let submit_controller = require("../controllers/submit_controller")
let front_controller = require("../controllers/front_controller")

// ROUTES
router.get('/', front_controller.get_all);
router.get('/submit/post', submit_controller.front_post_view);
router.get('/submit/topic', submit_controller.topic_view);
router.post('/submit/post', submit_controller.front_post);
router.post('/submit/topic', submit_controller.topic);

module.exports = router;