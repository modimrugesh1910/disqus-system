const express = require("express");
const router = express.Router();

// CONTROLLERS
let topic_controller = require("../controllers/topic_controller")
let comment_controller = require("../controllers/comment_controller")
let submit_controller = require("../controllers/submit_controller")

// ROUTES
router.get('/:topic', topic_controller.get_all);
router.get('/:topic/:id/comments', topic_controller.get_post);
router.get('/:topic/submit/post', submit_controller.topic_post_view);

router.post('/:topic/submit/post', submit_controller.topic_post);
router.post('/:topic/:id/comments', comment_controller.comment);

router.get('/:topic/:id', function (req, res) {
    res.redirect(`/r/${req.params.topic}/${req.params.id}/comments`)
});

module.exports = router